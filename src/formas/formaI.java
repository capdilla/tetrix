package formas;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Panel;
import java.util.HashMap;

import other.SlideLeft;
import other.Tablero;

public class formaI extends myContainer {
	// {{x},{y}}
	private int[][][] patron0 = { {}, { { -1 }, { -2 }, { 1 } } };
	private int[][][] patron90 = { { { -1 }, { 1 }, { 2 } }, {} };
	private int[][][] patron180 = patron0;
	private int[][][] patron270 = patron90;
	private static Color c = new Color(40, 183, 121);

	public formaI(int grados, Tablero t, int panelId, SlideLeft sl) {

		super(grados, t, panelId, sl);

		this.patrones.put(0, patron0);
		this.patrones.put(90, patron90);
		this.patrones.put(180, patron180);
		this.patrones.put(270, patron270);

	}

	@Override
	public void render0() {
		setLayout(new GridLayout(4, 0));

		for (int i = 0; i < 4; i++) {
			add(new Forma(c));
		}

	}

	@Override
	public void render90() {
		setLayout(new GridLayout(0, 4));

		for (int i = 0; i < 4; i++) {
			add(new Forma(c));
		}
	}

	@Override
	public void render180() {
		render0();
	}

	@Override
	public void render270() {
		render90();
	}

	@Override
	public Color getColor() {
		return this.c;
	}

}
