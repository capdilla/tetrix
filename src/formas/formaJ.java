package formas;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.HashMap;

import javax.swing.table.TableColumn;

import other.SlideLeft;
import other.Tablero;

public class formaJ extends myContainer {

	private static Color c = new Color(34, 85, 164);

	public formaJ(int angulo, Tablero t,int panelId,SlideLeft sl) {

		super(angulo, t,panelId,sl);
		int[][][] patron0 = { { { -1 } }, { { -1 }, { -2 } } };
		int[][][] patron90 = { { { 1 }, { 2 } }, { { -1 } } };
		int[][][] patron180 = { { { 1 } }, { { 1 }, { 2 } } };
		int[][][] patron270 = { { { -1 }, { -2 } }, { { 1 } } };

		this.patrones.put(0, patron0);
		this.patrones.put(90, patron90);
		this.patrones.put(180, patron180);
		this.patrones.put(270, patron270);
	}

	@Override
	public void render0() {

		setLayout(new GridLayout(3, 3));

		add(new Forma());
		add(new Forma(c));
		add(new Forma());
		add(new Forma(c));
		add(new Forma(c));
		add(new Forma(c));

	}

	@Override
	public void render90() {
		setLayout(new GridLayout(2, 3));

		add(new Forma(c));
		add(new Forma());
		add(new Forma());
		add(new Forma(c));
		add(new Forma(c));
		add(new Forma(c));

	}

	@Override
	public void render180() {

		setLayout(new GridLayout(3, 2));

		add(new Forma(c));
		add(new Forma(c));
		add(new Forma(c));
		add(new Forma());
		add(new Forma(c));
		add(new Forma());
	}

	@Override
	public void render270() {
		setLayout(new GridLayout(2, 3));

		add(new Forma(c));
		add(new Forma(c));
		add(new Forma(c));
		add(new Forma());
		add(new Forma());
		add(new Forma(c));

	}

	@Override
	public Color getColor() {
		// TODO Auto-generated method stub
		return this.c;
	}

}
