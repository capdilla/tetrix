package formas;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.HashMap;

import javax.swing.table.TableColumn;

import other.SlideLeft;
import other.Tablero;

public class formaS extends myContainer {
	private static Color c = new Color(39, 169, 227);

	public formaS(int angulo, Tablero t,int panelId,SlideLeft sl) {
		super(angulo, t,panelId,sl);

		int[][][] patron0 = { { { 1 } }, { { 1, -1 }, } };
		int[][][] patron90 = { { { 1, 1 }, }, { { -1 } } };

		this.patrones.put(0, patron0);
		this.patrones.put(90, patron90);
		this.patrones.put(180, patron0);
		this.patrones.put(270, patron90);
	}

	@Override
	public void render0() {
		setLayout(new GridLayout(2, 3));

		add(new Forma());
		add(new Forma(c));
		add(new Forma(c));
		add(new Forma(c));
		add(new Forma(c));
		add(new Forma());

	}

	@Override
	public void render90() {
		setLayout(new GridLayout(3, 2));

		

		add(new Forma(c));
		add(new Forma());
		add(new Forma(c));
		add(new Forma(c));
		add(new Forma());
		add(new Forma(c));

	}

	@Override
	public void render180() {
		render0();

	}

	@Override
	public void render270() {
		render90();

	}

	@Override
	public Color getColor() {
		return this.c;
	}

}
