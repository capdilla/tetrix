package formas;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.geom.Dimension2D;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import other.SlideLeft;
import other.Tablero;

/**
 * 
 * Clase que se utiliza para que todas las formas adopten esta clase
 *
 */
public abstract class myContainer extends JPanel {

	protected HashMap<Integer, int[][][]> patrones = new HashMap<>();
	private Tablero t;

	public myContainer(int grados, Tablero t, int panelId, SlideLeft sl) {

		this.t = t;

		this.setLocation(55, 50);
		setBackground(new Color(255, 255, 255, 100));

		render(grados);

		// se agrega un mouse listener
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				// se llamara al methodo setPatorn de tablero
				t.setPatron(getPatron(grados), getColor(), panelId);

				// llamamos a slideLeft y marcamos seleccionado el panel que se
				// le ha dado click
				sl.setBorderTo(panelId);
			}
		});

	}

	/**
	 * renderiza la figura con el angulo generado aleatoriamente desde la clase
	 * SlideLeft
	 * 
	 * @param grados
	 * 
	 */
	private void render(int grados) {
		switch (grados) {
		case 0:
			render0();
			break;
		case 90:
			render90();
			break;
		case 180:
			render180();
			break;
		case 270:
			render270();
			break;
		}
	}

	/*
	 * retorna el color de la figura
	 */
	public abstract Color getColor();

	/**
	 * renderiza la figura con angulo de 0 grados
	 */
	public abstract void render0();

	/**
	 * renderiza la figura con angulo de 90 grados
	 */
	public abstract void render90();

	/**
	 * renderiza la figura con angulo de 180 grados
	 */
	public abstract void render180();

	/**
	 * renderiza la figura con angulo de 270 grados
	 */
	public abstract void render270();

	/**
	 * retorna un haspmap de los patrones para dibujar
	 * 
	 * @return this.patrones
	 */
	public HashMap<Integer, int[][][]> getPatrones() {
		return this.patrones;
	}

	/**
	 * sirve para devolver el patron que tiene actualmente la forma
	 * 
	 * @param grados
	 * @return
	 */
	public int[][][] getPatron(int grados) {
		return getPatrones().get(grados);
	}

}
