package formas;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Panel;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * esta clase Forma, es la clase generia para generar todas las fromas del juego
 */
public class Forma extends JPanel {

	private Color c;

	public Forma() {
		// TODO Auto-generated constructor stub
		init(new Color(255, 255, 255, 100));
	}

	public Forma(Color c) {
		init(c);
	}

	private void init(Color c) {
		this.c = c;
		setBackground(c);
		setBorder(BorderFactory.createLineBorder(Color.gray));
		setPreferredSize(new Dimension(50, 50));
	}

	/**
	 * sirve para reiniciar el color de la forma a su color dado al inicio
	 */
	public void resetBackgroudColor() {
		setBackground(this.c);
	}

	/**
	 * sirve para retornar el color de la forma
	 * 
	 * @return
	 */
	public Color getColor() {
		return this.c;
	}

}
