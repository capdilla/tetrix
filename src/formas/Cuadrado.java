package formas;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.Panel;
import java.util.HashMap;

import other.SlideLeft;
import other.Tablero;

public class Cuadrado extends myContainer {
	// {{ {x,y},{x,y} },{ {y,x},{y,x} }}
	private int[][][] patron0 = { { { 1 } }, { { 1, 1 } } };
	private static Color c = new Color(27, 177, 254);

	public Cuadrado(int angulo, Tablero t, int panelId, SlideLeft sl) {
		super(angulo, t, panelId, sl);

		this.patrones.put(0, patron0);
		this.patrones.put(90, patron0);
		this.patrones.put(180, patron0);
		this.patrones.put(270, patron0);
	}

	@Override
	public void render0() {
		setLayout(new GridLayout(2, 2));
		for (int i = 0; i < 4; i++) {
			add(new Forma(c));
		}

	}

	@Override
	public void render90() {
		render0();

	}

	@Override
	public void render180() {
		render0();

	}

	@Override
	public void render270() {
		render0();

	}

	@Override
	public Color getColor() {
		return this.c;
	}

}
