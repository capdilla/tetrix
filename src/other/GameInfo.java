package other;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/*
 * esta clase es todo el cuadro donde se ve la informacion de la partida, Score y tiempo
 */
public class GameInfo extends JPanel {
	private Label userName, ScoreL, Time;
	private int minutes = 300;
	private int score = 0;

	public GameInfo(Ventana v) {

		setLayout(new GridLayout(3, 1));

		userName = new Label("Tetrix Uneatlantico ");
		userName.setFont(new Font(Font.DIALOG_INPUT, Font.PLAIN, 22));

		ScoreL = new Label("Score " + score);
		ScoreL.setFont(new Font(Font.DIALOG_INPUT, Font.PLAIN, 22));

		Time = new Label(this.ParseTime(minutes));
		Time.setFont(new Font(Font.DIALOG_INPUT, Font.BOLD, 18));

		add(userName);
		add(ScoreL);
		add(Time);
		setBackground(new Color(255, 255, 255, 100));

		// funcion para restar el tiempo
		Timer t = new Timer(1000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				minutes--;
				if (minutes == 0) {
					((Timer) e.getSource()).stop();
					JOptionPane.showMessageDialog(null, "Te has quedado sin Tiempo :( , tu score fue de : " + score);
					v.dispose();
				}
				Time.setText(ParseTime(minutes));
			}
		});
		t.start();

	}

	/**
	 * sirve para convertir 300 segudos a 5:00 minutos
	 * 
	 * @param minutes
	 * @return
	 */
	public String ParseTime(int minutes) {
		String startTime = "00:00";
		int h = minutes / 60 + Integer.parseInt(startTime.substring(0, 1));
		int m = minutes % 60 + Integer.parseInt(startTime.substring(3, 4));
		String newtime = h + ":" + m;
		return newtime;
	}

	/*
	 * incrementa el score, este metodo es llamado desde la Clase Tablero, y se
	 * le incrementan 10 al tiempo cada vez que se incremanta el score
	 */
	public void setScore(int score) {
		this.score = score;
		this.minutes = this.minutes + 10;
		ScoreL.setText("Score " + this.score);
	}

}
