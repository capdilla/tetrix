package other;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * 
 * Esta clase es la ventana del menu principal
 *
 */
public class ventanaInicio extends JFrame {
	private JButton btnJugar, btnSalir;

	public ventanaInicio() {
		
		btnJugar = new JButton("Jugar Ahora");
		btnSalir = new JButton("Salir");
		btnJugar.setBounds(125, 120, 150, 50);
		btnSalir.setBounds(125, 190, 150, 50);
		setVisible(true);
		//String ruta = "./ProyectoImagen.jpg";

		setContentPane(new JLabel(new ImageIcon(this.getClass().getResource("/other/ProyectoImagen.jpg"))));

		// this.repaint();

		addWindowListener(new WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				dispose();
			};
		});

		btnSalir.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				System.exit(0);
			}
		});

		btnJugar.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				System.getProperty("user.dir");
				new Ventana();
				// dispose();
			}
		});

		add(btnJugar);
		add(btnSalir);
		setLayout(null);
		setSize(400, 360);
		setLocationRelativeTo(null);
		setResizable(false);
		

	}
}
