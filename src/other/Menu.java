package other;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * 
 * esta es la clase menu que es la barra superior que se encuentra en la ventana
 * de juego
 *
 */
public class Menu extends JPanel {

	public Menu(Ventana v) {
		setBorder(BorderFactory.createLineBorder(Color.black));
		setPreferredSize(new Dimension(200, 50));
		setBackground(new Color(46, 54, 63));
		
		setLayout(new GridLayout(1,2));
		Button inicio = new Button("Inicio");
		Button Salir = new Button("Salir");
		add(inicio);
		add(Salir);

		inicio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				v.dispose();
			}
		});

		Salir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
	}
}
