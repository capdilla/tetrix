package other;

import java.awt.Color;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import formas.Forma;

/*
 * esta clase es cada recuadro del tablero donde se colocan las formas del juego
 */
public class cuadroJuego extends Forma {

	private int PositionX, PositionY;// posicion en el array de los cuadros
	private boolean isBusy = false;

	public cuadroJuego(Color c, Tablero t, int Y, int X) {

		super(c);
		this.PositionX = X;
		this.PositionY = Y;

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				if (!isBusy) {
					setBackground(Color.blue);
					t.paintShape(PositionY, PositionX);
				}

			}
		});

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				if (!isBusy) {
					resetBackgroudColor();
					t.restarShapeColor(PositionY, PositionX);
				}

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				t.setBusyToShape(PositionY, PositionX);
			}
		});

	}

	/**
	 * sirve para poner el cuadro como Ocupado
	 * 
	 * @param b
	 */
	public void setIsBusy(Boolean b) {
		this.isBusy = b;
	}
	
	/**
	 * retorna el valor para saber si el cuadro esta ocupado
	 * @return isBusy
	 */
	public Boolean getIsBusy() {
		return this.isBusy;
	}

}
