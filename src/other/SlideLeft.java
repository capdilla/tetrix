package other;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.util.HashMap;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import formas.*;

/*
 * en esta clase se muestran las figuras que se generan aleatoriamente y tambien se muestra la clase GameInfo
 * 
 */
public class SlideLeft extends JPanel {
	private Tablero t;
	private JPanel[] panels = new JPanel[4];
	public GameInfo GI;
	private HashMap<Integer, int[][][]> FormasPatrones;// sirve para almacenar los patrones de todas las formas generadas

	public SlideLeft(Ventana v, Tablero t) {

		this.t = t;
		FormasPatrones = new HashMap<>();

		setLayout(new GridLayout(4, 1));
		for (int i = 0; i < panels.length; i++) {
			panels[i] = new JPanel();
			panels[i].setLayout(new GridBagLayout());
			panels[i].setBackground(new Color(86, 99, 113));
			add(panels[i]);
		}

		GI = new GameInfo(v);
		panels[0].add(GI);

		panels[1].add(this.generarForma(1));
		panels[2].add(this.generarForma(2));
		panels[3].add(this.generarForma(3));

		setBorder(BorderFactory.createLineBorder(Color.gray));
		setPreferredSize(new Dimension(250, 100));
		setBackground(new Color(86, 99, 113));
	}

	/**
	 * sirve para quitarle los bordes a todos los paneles
	 */
	public void removeBorders() {
		panels[1].setBorder(BorderFactory.createLineBorder(new Color(86, 99, 113)));
		panels[2].setBorder(BorderFactory.createLineBorder(new Color(86, 99, 113)));
		panels[3].setBorder(BorderFactory.createLineBorder(new Color(86, 99, 113)));
	}

	/**
	 * sirve para resaltar la figura seleccionada
	 * 
	 * @param i
	 */
	public void setBorderTo(int i) {
		this.removeBorders();
		panels[i].setBorder(BorderFactory.createLineBorder(Color.YELLOW));
	}

	/**
	 * quita todo el contenido de panels[index] y genera una nueva forma
	 * 
	 * @param id
	 */
	public void destroyPanelContent(int id) {
		if (id > 0) {
			panels[id].removeAll();
			panels[id].setBorder(BorderFactory.createLineBorder(new Color(86, 99, 113)));
			panels[id].updateUI();
			panels[id].add(generarForma(id));
		}

	}

	/*
	 * Genera la forma y los angulos aleatoriamente
	 * 
	 */
	private myContainer generarForma(int panelId) {
		int[] angulos = { 0, 90, 180, 270 };
		int forma = (int) Math.floor(Math.random() * (6 - 0 + 1) + (0));

		//
		int ranAng = (int) Math.floor(Math.random() * ((angulos.length - 1) - 0 + 1) + (0));
		int angulo = angulos[ranAng];

		myContainer mc = null;

		switch (forma) {
		case 0:
			mc = new Cuadrado(angulo, t, panelId, this);
			break;
		case 1:
			mc = new formaI(angulo, t, panelId, this);
			break;
		case 2:
			mc = new formaJ(angulo, t, panelId, this);
			break;
		case 3:
			mc = new formaL(angulo, t, panelId, this);
			break;
		case 4:
			mc = new formaS(angulo, t, panelId, this);
			break;
		case 5:
			mc = new formaZ(angulo, t, panelId, this);
			break;
		case 6:
			mc = new formaT(angulo, t, panelId, this);
			break;
		}

		FormasPatrones.put(panelId, mc.getPatron(angulo));
		return mc;
	}

	public HashMap<Integer, int[][][]> getFormasPatrones() {
		return this.FormasPatrones;
	}

}
