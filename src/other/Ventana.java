package other;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Ventana extends Frame {
		
	public Ventana() {
		setLayout(new BorderLayout());
		
		Tablero t = new Tablero(this);
		SlideLeft sl = new SlideLeft(this,t);
		Menu menu = new Menu(this);
		add(menu,BorderLayout.NORTH);
		add(t,BorderLayout.CENTER);
		add(sl,BorderLayout.EAST);
		
		t.setSlideLeft(sl);
		
		setSize(900,900);
		setVisible(true);
		
		setResizable(false);
		setLocationRelativeTo(null);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				dispose();
			}
		});
	}
	
}
