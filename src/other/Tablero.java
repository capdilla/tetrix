package other;

import java.awt.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.print.attribute.standard.RequestingUserName;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.omg.PortableServer.AdapterActivator;

import formas.Forma;

/**
 * 
 * Componente principal donde esta toda la logica del juego, y donde se muestra
 * los patrones de las figuras
 *
 */
public class Tablero extends JPanel {
	private cuadroJuego[][] cJuego;
	public int row, colum;

	private int[][][] patronDefault = { { { 0 } }, { { 0 } } };
	private int[][][] p = patronDefault;
	private Color colorSelected;
	private int panelIdSelected;
	private SlideLeft sl;
	private Ventana v;

	private int puntos = 0;

	public Tablero(Ventana v) {

		this.row = 10;
		this.colum = 10;
		this.cJuego = new cuadroJuego[row][colum];
		this.v = v;

		this.llenarForma();

		setLayout(new GridLayout(row, colum));

		for (Forma[] forma : cJuego) {
			for (Forma forma2 : forma) {
				add(forma2);
			}
		}

		setBackground(new Color(46, 54, 63));

	}

	/*
	 * crea las nuevas instancias de cuadroJuego
	 */
	private void llenarForma() {

		for (int i = 0; i < cJuego.length; i++) {
			for (int j = 0; j < cJuego[i].length; j++) {
				cJuego[i][j] = new cuadroJuego(new Color(46, 54, 63), this, i, j);
			}
		}

	}

	/*
	 * se llama desde la Clase Ventana
	 * 
	 * @params SlideLeft ls
	 */
	public void setSlideLeft(SlideLeft sl) {
		this.sl = sl;
	}

	/**
	 * sirve para verificar si la forma se puede crear
	 * 
	 * @param y
	 * @param x
	 * @param patron
	 * @return
	 */
	private Boolean shapeCan(int y, int x, int[][][] patron) {

		if (this.cJuego[y][x].getIsBusy()) {
			return false;
		}
		// x
		for (int f = 0; f < patron[0].length; f++) {
			int xx = (x + patron[0][f][0]);

			if (xx >= 0 && xx < row) {
				if (!this.cJuego[y][xx].getIsBusy()) {

					for (int i = 0; i < patron[0][f].length; i++) {
						int yy = (y + patron[0][f][i]);
						if (yy >= 0 && yy < colum && i > 0)
							if (this.cJuego[yy][xx].getIsBusy()) {
								return false;
							}
					}
				} else {
					return false;
				}

			} else {
				return false;
			}
		}

		// y
		for (int f = 0; f < patron[1].length; f++) {
			int yy = (y + patron[1][f][0]);

			if (yy >= 0 && yy < colum) {
				if (!this.cJuego[yy][x].getIsBusy()) {

					for (int i = 0; i < patron[1][f].length; i++) {
						int xx = (x + patron[1][f][i]);
						if (xx >= 0 && xx < row && i > 0)
							if (this.cJuego[yy][xx].getIsBusy()) {
								return false;
							}
					}
				} else {
					return false;
				}

			} else {
				return false;
			}
		}
		return true;
	}

	/*
	 * se utiliza para colocar cada cuadro en modo ocupado
	 * 
	 * @params int y
	 * 
	 * @params int x
	 */
	public void setBusyToShape(int y, int x) {

		canContinuePlay();// verifica si aun puede continuar jugando
		if (!shapeCan(y, x, this.p)) {
			return;
		}

		if (this.p == patronDefault && colorSelected == null) {
			return;
		}

		this.sl.destroyPanelContent(this.panelIdSelected);
		this.cJuego[y][x].setIsBusy(true);
		this.cJuego[y][x].setBackground(colorSelected);
		// x
		for (int f = 0; f < p[0].length; f++) {
			int xx = (x + p[0][f][0]);

			if (xx >= 0 && xx < row) {
				this.cJuego[y][xx].setIsBusy(true);
				this.cJuego[y][xx].setBackground(colorSelected);

				for (int i = 0; i < p[0][f].length; i++) {
					int yy = (y + p[0][f][i]);
					if (yy >= 0 && yy < colum && i > 0) {
						this.cJuego[yy][xx].setIsBusy(true);
						this.cJuego[yy][xx].setBackground(colorSelected);
					}
				}
			}
		}

		// y
		for (int f = 0; f < p[1].length; f++) {
			int yy = (y + p[1][f][0]);

			if (yy >= 0 && yy < colum) {
				this.cJuego[yy][x].setBackground(colorSelected);
				this.cJuego[yy][x].setIsBusy(true);

				for (int i = 0; i < p[1][f].length; i++) {
					int xx = (x + p[1][f][i]);
					if (xx >= 0 && xx < row && i > 0) {
						this.cJuego[yy][xx].setIsBusy(true);
						this.cJuego[yy][xx].setBackground(colorSelected);
					}
				}
			}
		}
		this.p = patronDefault;
		this.colorSelected = null;
		this.panelIdSelected = 0;

		checkLines();
	}

	/**
	 * methodo llamado desde
	 * 
	 * @param patron
	 * @param c
	 * @param panelId
	 */
	public void setPatron(int[][][] patron, Color c, int panelId) {
		this.p = patron;
		this.colorSelected = c;
		this.panelIdSelected = panelId;
	}

	/*
	 * revisa todo el arreglo de cJuego linea por linea para verificar si todas
	 * las lineas estan llenas
	 */
	private void checkLines() {

		for (int i = 0; i < cJuego.length; i++) {
			Boolean line = true;

			// ver si la fila esta completa
			for (int j = 0; j < cJuego[i].length; j++) {
				if (!cJuego[i][j].getIsBusy()) {
					line = false;
					break;
				}
			}

			if (line) {
				for (int j = 0; j < cJuego.length; j++) {
					cJuego[i][j].setIsBusy(false);
					cJuego[i][j].resetBackgroudColor();
				}
				this.puntos = this.puntos + 10;
				sl.GI.setScore(this.puntos);
			}

		}

		canContinuePlay();

	}

	/**
	 * revisa todo el tablero para ver si se pueden colocar aun piezas
	 */
	private void canContinuePlay() {

		Iterator<Entry<Integer, int[][][]>> it = this.sl.getFormasPatrones().entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<java.lang.Integer, int[][][]> patron = (Map.Entry<java.lang.Integer, int[][][]>) it.next();

			for (int y = 0; y < cJuego.length; y++) {
				for (int x = 0; x < cJuego.length; x++) {
					if (shapeCan(y, x, patron.getValue())) {
						// System.out.println("y : "+ y + " X : "+ x + "patron
						// "+ patron.getKey());
						return;
					}
				}
			}
		}

		JOptionPane.showMessageDialog(null, "Te has quedado sin movimientos :( , tu puntuacion fue de :" + this.puntos);
		this.v.dispose();
	}

	/*
	 * sirve para colocar el color en el cJuego[y][x] segun el patron
	 * seleccionado
	 * 
	 * @params int y,x
	 * 
	 * @params Color c
	 */

	public void setColor(int y, int x, Color c) {
		// x
		for (int f = 0; f < p[0].length; f++) {
			int xx = (x + p[0][f][0]);

			if (xx >= 0 && xx < row) {
				if (!this.cJuego[y][xx].getIsBusy()) {
					this.cJuego[y][xx].setBackground(c);
					for (int i = 0; i < p[0][f].length; i++) {
						int yy = (y + p[0][f][i]);
						if (yy >= 0 && yy < colum && i > 0)
							if (!this.cJuego[yy][xx].getIsBusy()) {
								this.cJuego[yy][xx].setBackground(c);
							}
					}
				}

			}
		}

		// y
		for (int f = 0; f < p[1].length; f++) {
			int yy = (y + p[1][f][0]);

			if (yy >= 0 && yy < colum) {
				if (!this.cJuego[yy][x].getIsBusy()) {
					this.cJuego[yy][x].setBackground(c);
					for (int i = 0; i < p[1][f].length; i++) {
						int xx = (x + p[1][f][i]);
						if (xx >= 0 && xx < row && i > 0)
							if (!this.cJuego[yy][xx].getIsBusy()) {
								this.cJuego[yy][xx].setBackground(c);
							}
					}
				}

			}
		}
	}

	/*
	 * pinta las figuras cuando el mouse se pocisiona encima de un cuadroJuego
	 */
	public void paintShape(int y, int x) {

		Color c = Color.BLUE;
		setColor(y, x, this.colorSelected);

	}

	/*
	 * restaura el cuadro juego a su color cuando el mouse sale del cuadro juego
	 */
	public void restarShapeColor(int y, int x) {
		setColor(y, x, new Color(46, 54, 63));
	}

}
